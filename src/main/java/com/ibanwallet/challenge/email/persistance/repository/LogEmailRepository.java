package com.ibanwallet.challenge.email.persistance.repository;

import com.ibanwallet.challenge.email.persistance.entity.LogEmail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogEmailRepository extends JpaRepository<LogEmail, Long> {
}
