package com.ibanwallet.challenge.email.service;

import com.ibanwallet.challenge.email.model.Email;
import com.ibanwallet.challenge.email.persistance.entity.LogEmail;
import com.ibanwallet.challenge.email.persistance.repository.LogEmailRepository;
import com.ibanwallet.challenge.email.types.EmailContentType;
import com.ibanwallet.challenge.email.types.StatusEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class LogEmailService {

    @Autowired
    private LogEmailRepository logEmailRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public LogEmail createPending(Email email) {
        LogEmail logEmail = new LogEmail();
        logEmail.setContent(email.getBody());
        logEmail.setFrom(email.getFrom());
        logEmail.setHtml(Optional.ofNullable(email.getType()).orElse(EmailContentType.TEXT) == EmailContentType.HTML);
        logEmail.setStatus(StatusEmail.PENDING);
        logEmail.setSubject(email.getSubject());
        logEmail.setTo(email.getTo());
        logEmail.setCreatedAt(LocalDateTime.now());
        logEmail.setUpdatedAt(LocalDateTime.now());

        return logEmailRepository.save(logEmail);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateSent(LogEmail logEmail) {
        logEmail.setStatus(StatusEmail.SENT);
        logEmail.setUpdatedAt(LocalDateTime.now());

        logEmailRepository.save(logEmail);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateError(LogEmail logEmail, Exception e) {
        logEmail.setStatus(StatusEmail.ERROR);
        logEmail.setUpdatedAt(LocalDateTime.now());
        logEmail.setError(e.getMessage());

        logEmailRepository.save(logEmail);
    }
}
