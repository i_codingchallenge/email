package com.ibanwallet.challenge.email.service;

import com.ibanwallet.challenge.email.exception.SendEmailException;
import com.ibanwallet.challenge.email.model.Email;
import com.ibanwallet.challenge.email.persistance.entity.LogEmail;
import com.ibanwallet.challenge.email.persistance.repository.LogEmailRepository;
import com.ibanwallet.challenge.email.types.EmailContentType;
import com.ibanwallet.challenge.email.types.StatusEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Optional;

@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class EmailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailService.class);

    @Autowired
    private LogEmailService logEmailService;

    @Autowired
    private JavaMailSender emailSender;

    @Transactional(propagation = Propagation.REQUIRED)
    public void sendEmail(Email email) throws SendEmailException {
        LogEmail logEmail = logEmailService.createPending(email);

        try {
            MimeMessage message = emailSender.createMimeMessage();

            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setTo(email.getTo());
            helper.setSubject(email.getSubject());
            helper.setText(email.getBody(),
                    Optional.ofNullable(email.getType()).orElse(EmailContentType.TEXT) == EmailContentType.HTML);
            helper.setFrom(email.getFrom());

            emailSender.send(message);

            logEmailService.updateSent(logEmail);
        } catch (Exception e) {
            LOGGER.error("Error on sending email to {} with subject {}", email.getTo(), email.getSubject(), e);

            logEmailService.updateError(logEmail, e);
            throw new SendEmailException(e);
        }
    }

}
