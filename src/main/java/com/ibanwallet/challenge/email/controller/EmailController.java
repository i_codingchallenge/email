package com.ibanwallet.challenge.email.controller;

import com.ibanwallet.challenge.email.exception.SendEmailException;
import com.ibanwallet.challenge.email.model.Email;
import com.ibanwallet.challenge.email.service.EmailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;

@RestController
@RequestMapping("/email")
@Api(value = "Email Services")
public class EmailController {

    @Autowired
    private EmailService emailService;

    @PostMapping(produces = "application/json")
    @ApiOperation("send email")
    public void send(@RequestBody Email email) throws SendEmailException {
        emailService.sendEmail(email);
    }

}
