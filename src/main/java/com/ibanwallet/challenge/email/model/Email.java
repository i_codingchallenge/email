package com.ibanwallet.challenge.email.model;

import com.ibanwallet.challenge.email.types.EmailContentType;

import java.util.List;

public class Email {

    private String from;

    private String to;

    private String subject;

    private String body;

    private EmailContentType type;

    private List<Attachment> attachments;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public EmailContentType getType() {
        return type;
    }

    public void setType(EmailContentType type) {
        this.type = type;
    }

    public List<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

}
