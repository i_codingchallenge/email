package com.ibanwallet.challenge.email.exception;

public class SendEmailException extends Exception {

    public SendEmailException(Throwable cause) {
        super("Error sending the email", cause);
    }

}
