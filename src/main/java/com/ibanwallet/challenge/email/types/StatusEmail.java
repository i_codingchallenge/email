package com.ibanwallet.challenge.email.types;

public enum StatusEmail {

    PENDING, SENT, ERROR

}
