package com.ibanwallet.challenge.email.types;

public enum EmailContentType {

    TEXT, HTML

}
