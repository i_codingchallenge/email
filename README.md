# Email API

> API for sending emails 

## Dependencies

* Java 8
* Maven
* Docker
* Docker Network
* SpringBoot

* Apache James for local email testing https://james.apache.org/

## Starting Apache James Local Server

```
docker container run --network iban-subscription --name james.local -p "25:25" -p "143:143" linagora/james-jpa-sample:3.4.0
```

Then add to hosts file:
```
127.0.0.1		james.local
``` 

This docker image has a default configuration which includes a domain named james.local and three users:
- user01 - user01@james.local
- user02 - user02@james.local
- user03 - user03@james.local

All the users have the password **1234**. The server respond with **IMAP port 143** and **SMTP port 25**. 

## Build

```
mvn clean install
```

## Local Deploy

```
java -jar target/email-0.0.1-SNAPSHOT.jar
```

http://localhost:8082/swagger-ui.html

## Deploy with Docker

**Important! Create the docker network if not created:**

```
docker network create iban-subscription
```

Then build the docker image and start the container:

```
docker image build -t email .
docker container run --network iban-subscription --name email -p 8082:8082 -d email
```
